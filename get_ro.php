<?php

// Get the year from the command line.
$year = $argv[1];

// Set the academic year based on the year.
$academic_year = $year . '-' . ($year + 1);

// Set the academic year in short form, used for
// redirects to the ucalendar site.
$short_academic_year = $academic_year[2] . $academic_year[3] . $academic_year[7] . $academic_year[8];

// Get the active date.
$active_date = '9/1/' . $year;

// The URLs to exclude from fixing certain things.
$url_exclusions = [
  '/Course-Descriptions-Index',
  '/page/uWaterloo-Calendar-Events-and-Academic-Deadlines',
];

// Create the academic year directory if it does not exist.
if (!file_exists($academic_year)) {
  mkdir($academic_year, 0777, true);
}

// Get the cookie to the ugradcalendar site.
$cookie = get_cookie($active_date);

// The URL to the ugradcalendar site.
$url = 'https://ugradcalendar.uwaterloo.ca/page/uWaterloo-Undergraduate-Calendar-Access?ActiveDate=' . $active_date;

// Get the data from the index page of the ugradcalendar site.
$data = get_remote_data($url, $cookie);

// Download the archive.
get_archive($data, $academic_year, $cookie);

// Apply all the fixes to the archive.
fix_archive($academic_year, $short_academic_year, $cookie);

/**
 *
 * @param string $academic_year
 * @param string $short_academic_year
 * @param string $cookie
 * @return void
 */
function get_archive(
  string $data,
  string $academic_year,
  string $cookie
) {

  global $url_exclusions;

  // Get the menu links for the ugradcalendar site.
  $menu_links = get_menu_links($data, $cookie, $academic_year);

  // Step through each of the first level menu links and get
  // the content.
  foreach ($menu_links as $index => $menu_level) {

    // Output current progress.
    uwprint('Starting menu level ' . $index . ' pages', TRUE);

    // Step through each of the actual menu links and process them./
    foreach ($menu_level as $menu_link) {

      // Output the current progress.
      uwprint('Converting ... ' . $menu_link);

      // For some reason certain links are to the root, for example:
      // /MATH-List-of-Academic-Programs-or-Plans, and the aspx knows
      // to convert this to /page/<url>, so we need to convert any
      // urls that are to the root to /page.
      if (
        count(explode('/', $menu_link)) == 2 &&
        !in_array($menu_link, $url_exclusions)
      ) {
        $menu_link = '/page' . $menu_link;
      }

      // Break the menu link into parts based on the /.
      $parts = explode('/', $menu_link);

      // If there are more than two parts, its means that there
      // is a /somedir/somepage, so we need to handle that first.
      if (count($parts) > 2) {

        // Get the path to the file name which includes the
        // academic year.
        $path = $academic_year . '/' . $parts[1];

        // If the path does not exist then create it.
        if (!file_exists($path)) {
          mkdir($path, 0777, true);
        }

        // Set the file name to be used.
        $file_name = $academic_year . '/' . $parts[1] . '/' . $parts[2] . '.html';
      }
      // This is on the root of the ugradcalendar site, so just
      // use the academic year.
      else {

        // Set the file name with only academic year.
        $file_name = $academic_year . '/' . $parts[1] . '.html';
      }

      // Get the url to menu link.
      $url = 'https://ugradcalendar.uwaterloo.ca' . $menu_link . '?ActiveDate=9/1/' . $academic_year;

      // Get the remote data from the ugradcalendar site.
      $data = get_remote_data($url, $cookie);

      // Step through the menu links and replace on the data.
      foreach ($menu_links as $mls) {

        // Step through each of the actual menu links and process them.
        foreach ($mls as $ml) {

          // Get the search pattern based on menu link, some root ones
          // we need to handle differently since the redirect happens
          // through on aspx side of the site.
          switch ($ml) {
            case '/page/uWaterloo-Calendar-Events-and-Academic-Deadlines':
              $search = 'href="/uWaterloo-Calendar-Events-and-Academic-Deadlines"';
              break;

            case '/page/uWaterloo-Undergraduate-Calendar-Access':
              $search = 'href="/uWaterloo-Undergraduate-Calendar-Access"';
              break;

            case '/page/Course-Descriptions-Index':
              $search = 'href="/Course-Descriptions-Index"';
              break;

            default:
              $search = 'href="' . $ml . '"';
              break;
          }

          // Set the replacement url, we need to add page
          // if the original url is to just the root.
          $replace = 'href="/' . $academic_year;
          if (
            count(explode('/', $ml)) == 2 &&
            !in_array($ml, $url_exclusions)
          ) {
            $replace .= '/page';
          }
          $replace .= $ml . '.html"';

          // Replace all the menu links in the data.
          $data = str_replace(
            $search,
            $replace,
            $data
          );
        }
      }

      // Open the file.
      $file = fopen($file_name, "w") or die("Unable to open file!");

      // Write the data to the file.
      fwrite($file, $data);

      // Close the file.
      fclose($file);
    }

    // Output current progress.
    uwprint('Completed ... menu level ' . $index . ' pages');
  }
}

/**
 * Function to apply all the fixes to the archive.
 *
 * @param string $academic_year
 *   The academic year.
 * @param string $short_academic_year
 *   The short academic year.
 * @param string $cookie
 *   The cookie.
 */
function fix_archive(
  string $academic_year,
  string $short_academic_year,
  string $cookie
) {

  // Output current progress.
  uwprint('Starting to fix archives ...', TRUE);

  // Reset the files array.
  $files = [];

  /*
  // To test single file uncomment these lines and change
  // the file name in the array.
  $files = [
    '2012-2013/page/SCI-Honours-Co-operative-Mathematical-Physics1.html',
  ];
  */

  // If there are no files, this means, we are doing a full
  // run, so we need to get all the files from the archive.
  if (empty($files)) {

    // Get the directories recursively.
    $rdi = new RecursiveDirectoryIterator(
      $academic_year,
      FilesystemIterator::KEY_AS_PATHNAME
    );

    // Step through each of the directories and get the files.
    foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {

      // If this is not a directory and not the . or .. file, then add
      // it to the files array.
      if (
        !is_dir($file) &&
        $info->getFileName() !== '.' &&
        $info->getFileName() !== '..'
      ) {

        $files[] = $file;
      }
    }
  }

  // Step through each of the files and change the links.
  foreach ($files as $file) {

    // Output current progress.
    uwprint('Fixing ... ' . $file);

    // Get the actual page from the path.
    $page = file_get_contents($file);

    // Fix the links on the page.
    $page = fix_links($page, $academic_year, $short_academic_year, $cookie, $file);

    // Remove the links on the page.
    $page = remove_links($page);

    // Open and write the file with updated data.
    $file = fopen($file, "w") or die("Unable to open file!");
    fwrite($file, $page);
    fclose($file);
  }

  // Remove the files that we no longer require.
  remove_files($academic_year);

  // Output current progress.
  uwprint('Completed ... fixing archives');
}

/**
 * Function to get the menu links.
 *
 * @param string $data
 *   The data of the page.
 * @param string $cookie
 *   The cookie.
 * @param string $academic_year
 *   The academic year.
 *
 * @return array
 *   Array of menu links.
 */
function get_menu_links(
  string $data,
  string $cookie,
  string $academic_year
): array {

  global $url_exclusions;

  // Step through levels 1 - 6 of menu links.
  $i = 1;
  do {

    // If this is the first menu level, we need to handle differently.
    if ($i == 1) {

      // Output current progress.
      uwprint('Starting level 1 menu links ...', TRUE);

      // The patterns to search for.
      $patterns = [
        '/<a\s[^>]*class="Level1"[^>]*>/s',
        '/<a\s[^>]*class="Level1Selected"[^>]*>/s',
      ];

      // Step through each of the patterns and get the menu links.
      foreach ($patterns as $pattern) {

        // Get all the links that are level 1 pattern.
        preg_match_all(
          $pattern,
          $data,
          $matches,
          PREG_SET_ORDER
        );

        // Step through all matches and get the menu link url.
        foreach ($matches as $match) {

          // Get the href from the "a" tag.
          $url = string_between_two_string($match[0], 'href="', '"');

          // Output current progress.
          uwprint('Fetching ... ' . $url);

          // Ensure that we are setting the correct url.  The three links listed here
          // in the if link to the rool but then are redirected on the ugradcalendar
          // site to /page/<url>, so we need to set that.
          if (
            $url == '/uWaterloo-Calendar-Events-and-Academic-Deadlines' ||
            $url == '/uWaterloo-Undergraduate-Calendar-Access' ||
            $url == '/Course-Descriptions-Index'
          ) {
            $url = '/page' . $url;
          }

          // Add the url to the urls array.
          $urls[$i][] = $url;
        }
      }

      // Output the current progress.
      uwprint('Completed level 1 menu links ...');
      uwprint();
    }
    else {

      // Output current progress.
      uwprint('Starting level ' . $i . ' menu links ...', TRUE);

      // Get the url level to run through, which is the current index
      // loop - 1, so for level 2 menu links, we will step through the
      // level 1 links and get all the level 2 links.
      $url_level = $i - 1;

      // If this is a not a level 4 link, the patterns are LevelNum and
      // LevelNumSelected for the patterns.
      // If it is a level 4, then the pattern is Level4Plus.
      if ($i < 4) {
        $patterns = [
          '/<a\s[^>]*class="Level' . $i . '"[^>]*>/s',
          '/<a\s[^>]*class="Level' . $i . 'Selected"[^>]*>/s'
        ];
      }
      else {
        $patterns = [
          '/<a\s[^>]*class="Level4Plus"[^>]*>/s',
          '/<a\s[^>]*class="Level4PlusSelected"[^>]*>/s'
        ];
      }

      $all_urls = [];
      foreach ($urls as $url_index) {
        foreach ($url_index as $url) {
          $all_urls[] = $url;
        }
      }

      // Step through each of the urls in the previous level and get the
      // urls in the current level.
      foreach ($urls[$url_level] as $url) {

        // For some reason certain links are to the root, for example:
        // /MATH-List-of-Academic-Programs-or-Plans, and the aspx knows
        // to convert this to /page/<url>, so we need to convert any
        // urls that are to the root to /page.
        if (
          count(explode('/', $url)) == 2 &&
          !in_array($url, $url_exclusions)
        ) {
          $url = '/page' . $url;
        }

        // Output current progress.
        uwprint('Fetching ... ' . $url);

        // The url to fetch.
        $full_url = 'https://ugradcalendar.uwaterloo.ca' . $url . '?ActiveDate=9/1/' . $academic_year;

        // Get the data of the url.
        $data = get_remote_data($full_url, $cookie);

        // Step through each of the patterns and get the menu links.
        foreach ($patterns as $pattern) {

          // Get all the menu links using the pattern.
          preg_match_all(
            $pattern,
            $data,
            $matches,
            PREG_SET_ORDER
          );

          // Step through all the matches and get the actual menu link.
          foreach ($matches as $match) {

            // Remove the links directly to the archive, i.e. those only with
            // ?ActiveDate=... and those to the direct calendar (we deal with
            // these links later).
            if (
              !str_contains($match[0], 'ActiveDate') &&
              !str_contains($match[0], 'www.ucalendar.uwaterloo.ca') &&
              !in_array($url, $url_exclusions)
            ) {

              $inserted_url = string_between_two_string($match[0], 'href="', '"');

              if (!in_array($inserted_url, $all_urls)) {
                $urls[$i][] = $inserted_url;
              }
            }
          }
        }
      }

      // Output the current progress.
      uwprint('Completed level ' . $i . ' menu links ...');
      uwprint();
    }

    $i++;
  } while (isset($urls[$i - 1]));

  return $urls;
}

/**
 * Function get the string between tow other strings.
 *
 * @param string $str
 *   The full string.
 * @param string $starting_word
 *   The starting string.
 * @param string $ending_word
 *   The ending string.
 *
 * @return string
 *   The string between, if there is any.
 */
function string_between_two_string(
  string $str,
  string $starting_word,
  string $ending_word
) {

  // Get the starting position.
  $subtring_start = strpos($str, $starting_word);

  // Adding the starting index of the starting word to
  // its length would give its ending index
  $subtring_start += strlen($starting_word);

  // Length of our required sub string
  $size = strpos($str, $ending_word, $subtring_start) - $subtring_start;

  // Return the substring from the index substring_start of length size
  return substr($str, $subtring_start, $size);
}

/**
 * Get the cookie of the ugradcalendar site.
 *
 * @param string $active_date
 *   The active date to use on the site.
 *
 * @return string
 *   The cookie.
 */
function get_cookie(string $active_date): string {

  // Have at least an empty string to return.
  $cookie = '';

  // Get the url to use to get the cookie.
  $url = 'https://ugradcalendar.uwaterloo.ca/group/uWaterloo-Admissions?ActiveDate=' . $active_date;

  // Get curl request to the site.
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HEADER, 1);

  // Perform the actual curl request.
  $result = curl_exec($ch);

  // Get the matches to the cookie.
  preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);

  // Step through each match and get the current cookie.
  foreach ($matches[1] as $item) {
    $cookie = $item;
  }

  return $cookie;
}

/**
 * Function to get the remote data.
 *
 * @param string $url
 *   The url to get.
 * @param string $cookie
 *   The cookie for the site.
 *
 * @return bool|string
 *   The data or false (can not get the data).
 */
function get_remote_data(string $url, string $cookie): bool|string {

  // Setup the actual curl request.
  $c = curl_init();
  curl_setopt($c, CURLOPT_URL, $url);
  curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($c, CURLOPT_COOKIE, $cookie);
  $headers[] = "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:76.0) Gecko/20100101 Firefox/76.0";
  $headers[] = "Pragma: ";
  $headers[] = "Cache-Control: max-age=0";
  curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($c, CURLOPT_MAXREDIRS, 10);
  curl_setopt($c, CURLOPT_FOLLOWLOCATION, FALSE);

  // Perform the curl request and close the connection.
  $data = curl_exec($c);
  curl_close($c);

  return $data;
}

/**
 * Function to fix the course links.
 *
 * @param string $path
 *   The path to the file.
 * @param string $academic_year
 *   The full academic year.
 * @param string $short_academic_year
 *   The short academic year.
 */
function fix_links(
  string $page,
  string $academic_year,
  string $short_academic_year,
  string $cookie,
  string $file_name
) {

  // Get all the "a" tags on the page.
  preg_match_all(
    '/<\s*a[^>]*href="(.*?)"\s?(.*?)>/s',
    $page,
    $atags
  );

  // Counter to be used for the search and replace patterns.
  $count = 0;
  $replacements = [];

  // Step through all the "a" tags and correct them.
  foreach ($atags[1] as $atag) {

    // Add any pages with version links to versions.txt
    if (str_contains($atag, '/version/')) {
      $file = 'versions.txt';
      $current = file_get_contents($file);
      $current .= $academic_year . ': ' . string_between_two_string($file_name, $academic_year . '/', '.html') . "\n";
      file_put_contents($file, $current);
    }

    /*
    // Add any pages with triple / links to links.txt
    if (str_contains($atag, ':///')) {
      $file = 'links.txt';
      $current = file_get_contents($file);
      $current .= $academic_year . ': ' . string_between_two_string($file_name, $academic_year . '/', '.html') . "\n";
      file_put_contents($file, $current);
    }
    */

    // Reset the parameters array.
    $parameters = [];

    // If this is a tag with courses.aspx, we need to generate
    // the redirected url using the pattern that the acms
    // generates.
    if (str_contains($atag, 'courses.aspx')) {

      // For some reason some tags have amp; in it, so
      // just remove it.
      $fixed_atag = str_replace('amp;', '', $atag);
      $fixed_atag = str_replace(':///', '://', $fixed_atag);

      // Use parse_url() function to parse the URL
      // and return an associative array which
      // contains its various components.
      $url_components = parse_url($fixed_atag);

      // Use parse_str() function to parse the
      // string passed via URL.
      parse_str($url_components['query'], $parameters);

      // Get the array keys of the parameters.
      $keys = array_map('strtolower', array_keys($parameters));
      $parameters = array_combine($keys, array_values($parameters));

      // If this contains a level, code and number, we need
      // to get the correct redirect.
      // For example: /courses.aspx?Level=100&Code=ANTH&Number=201,
      // will go to: COURSE/course-ANTH.html#ANTH201
      if (
        in_array('level', $keys) &&
        in_array('code', $keys) &&
        in_array('number', $keys)
      ) {

        // Setup the correct redirect.
        $redirect = 'https://ucalendar.uwaterloo.ca/' . $short_academic_year;
        $redirect .= '/COURSE/course-' . $parameters['code'] . '.html';
        $redirect .= '#' . $parameters['code'] . $parameters['number'];

        // Set the search and replace.
        $replacements['href="' . $atag . '"'] = 'href="' . $redirect . '"';

        // Increment the counter.
        $count++;
      }

      // If this contains a level and code, we need
      // to get the correct redirect.
      // For example: /courses.aspx?Level=100&Code=ANTH,
      // will go to: COURSE/course-ANTH.html#ANTH100
      elseif (
        in_array('level', $keys) &&
        in_array('code', $keys)
      ) {

        // Setup the correct redirect.
        $redirect = 'https://ucalendar.uwaterloo.ca/' . $short_academic_year;
        $redirect .= '/COURSE/course-' . $parameters['code'] . '.html';
        $redirect .= '#' . $parameters['code'] . $parameters['level'] . 'S';

        // Set the search and replace.
        $replacements['href="' . $atag . '"'] = 'href="' . $redirect . '"';

        // Increment the counter.
        $count++;
      }
    }

    // If the tag contains a link directly to the courses, we need
    // to generate the correct url.
    // Example 1: /courses/CLAS/221
    // Goes to: COURSE/course-CLAS.html#CLAS221.
    // Example 2: /courses/ANTH/level/300
    // Goes to: COURSE/course-ANTH.html#ANTH300S.
    // Example 3: /courses/AFM.
    // Goes to: COURSE/course-AFM.html
    elseif (str_contains($atag, '/courses/')) {

      // Get the parts of the tag.
      $parts = explode('/', str_replace(
        ['https://ugradcalendar.uwaterloo.ca', '/' . $academic_year, '.html'],
        '',
        $atag
      ));

      // Setup the first part of the redirect, this will be the same
      // for examples 1/2 and is the final redirect for example 3.
      $redirect = 'https://ucalendar.uwaterloo.ca/' . $short_academic_year;
      $redirect .= '/COURSE/course-' . $parts[2] . '.html';

      // If there are 4 parts it is like example 1, so set the
      // correct redirect.
      if (count($parts) == 4) {
        $redirect .= '#' . $parts[2] . $parts[3];
      }

      // If there are 5 parts, it is like example 2, so set the
      // correct redirect.
      if (count($parts) == 5) {
        $redirect .= '#' . $parts[2] . $parts[4] . 'S';
      }

      // Set the search and replace.
      $replacements['href="' . $atag . '"'] = 'href="' . $redirect . '"';

      // Increment the counter.
      $count++;
    }
    // If this is a tag to the default, process it.
    elseif (str_contains($atag, 'default.aspx')) {

      // Get the redirect using the get_redirect function.
      $redirect = '/' . $academic_year;
      $redirect .= get_redirect('https://ugradcalendar.uwaterloo.ca' . $atag, $cookie) . '.html';

      // Set the search and replace.
      $replacements['href="' . $atag . '"'] = 'href="' . $redirect . '"';

      // Increment the counter.
      $count++;
    }

    // If tis is a tag contains https://ugradcalendar.uwaterloo.ca
    // replace it with /$academic_year.
    elseif (str_contains($atag, 'https://ugradcalendar.uwaterloo.ca')) {

      // Set the redirect.
      $redirect = str_replace(
        'https://ugradcalendar.uwaterloo.ca',
        '/' . $academic_year,
        $atag
      );

      // Set the search and replace.
      $replacements['href="' . $atag . '"'] = 'href="' . $redirect . '.html"';

      // Increment the counter.
      $count++;
    }

    // If there are still urls with without a .html, we need to check
    // if there are pointing to ugradcalendar.uwaterloo.ca and if so
    // we need to fix those links.
    elseif (!str_contains($atag, '.html')) {

      // If the url contains link back to the calendar, point it back
      // to the local site.
      if (str_contains($atag, 'http://ugradcalendar.uwaterloo.ca')) {

        // Remove the link to the calendar.
        $redirect = str_replace(
          'http://ugradcalendar.uwaterloo.ca',
          '',
          $atag
        );

        // If there is a /page/ or /group/, add to the replacements.
        if (
          str_contains($redirect, '/page/') ||
          str_contains($redirect, '/group/')
        ) {
          $replacements[$atag] = '/' . $academic_year . $redirect . '.html';
        }
      }

      // If there are still links to /page or /group that do not have
      // and .html extension, add it to the replacements.
      elseif (
        str_contains($atag, '/page/') ||
        str_contains($atag, '/group/')
      ) {
        $replacements[$atag] = '/' . $academic_year . $atag . '.html';
      }
    }
  }

  // Step through all the searches and replaces, and replace
  // the tag with the redirect.
  foreach ($replacements as $search => $replace) {
    $page = str_replace(
      $search,
      $replace,
      $page
    );
  }

  return $page;
}

/**
 * Function to get the redirect actual value.
 *
 * @param string $url
 *   The url to process.
 * @param string $cookie
 *   The cookie to set
 *
 * @return string
 *   The redirected url.
 */
function get_redirect(string $url, string $cookie): string {

  // Setup the curl request.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_COOKIE, $cookie);
  curl_setopt(
    $ch,
    CURLOPT_USERAGENT,
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
  );
  curl_setopt($ch, CURLOPT_HEADER, true);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  // Execute the curl request and close the request.
  $response = curl_exec($ch);
  curl_close($ch);

  // Have at least empty url to return.
  $url = '';

  // If there are redirects, process them.
  if (preg_match_all("/^Location: (.*?)$/im", $response, $results)) {

    $url = $results[1][0];
  }

  return $url;
}

/**
 * Function to remove certain links from the page.
 *
 * @param string $page
 *   The page.
 *
 * @return array|string|string[]
 *   The page with links removed.
 */
function remove_links(string $page) {

  // The regular expression patterns to remove the links.
  $patterns = [
    '#<\s*a[^>]*href="\/page\/printable.*?"\s?.*?>.*?<\/a>#s',
    '#<\s*span[^>]*\s?.*?>.*?<input[^>]*>.*?Advanced Search.*?or.*?<\/span>#s',
    '#<\s*a[^>]*id="ctl00_uxResetCalendar"\s?.*?>.*?<\/a>#s',
    '#<li[^>]*><a[^>]*>.*?List of Undergraduate Studies Archived Calendars.*?<\/a><\/li>#s',
    '#<li[^>]*><a[^>]*>PDF Files<\/a>.*?<\/li>#s',
    '#<li[^>]*>To view a PDF.*?<a[^>]*>Available PDFs<\/a>.*?<\/li>#s',
    '#To view a [C,c]alendar from previous years.*?<a[^>]*>.*?List of Undergraduate Studies Archived Calendars.*?<\/a>\.?#s',
    '#Use the <b>Search bar<\/b> to search the current Calendar.*?<a[^>]*>.*?<\/a>.*?archived Calendar\.#s',
  ];

  // Step through each of the patterns and remove the links.
  foreach ($patterns as $pattern) {

    // Reset the matches array.
    $matches = [];

    // Find all the matches based on the pattern.
    preg_match_all(
      $pattern,
      $page,
      $matches
    );

    // If there are matches then replace them with blank string.
    if (isset($matches[0])) {

      // Step through each of the matches and replace with blank string.
      foreach ($matches[0] as $match) {
        $page = str_replace($match, '', $page);
      }
    }
  }

  return $page;
}

/**
 * Function to remove certain files.
 *
 * @param string $academic_year
 *   The academic year.
 */
function remove_files(string $academic_year): void {

  // Files to delete.
  $files = [
    $academic_year . '/group/uWaterloo-List-of-Undergraduate-Calendars.html',
    $academic_year . '/group/uWaterloo-Undergraduate-Calendar-PDF-Files.html',
  ];

  // Step through and delete each of the files.
  foreach ($files as $file_name) {
    if (file_exists($file_name)) {
      unlink($file_name);
    }
  }
}

/**
 * Function to print a message.
 *
 * @param string|null $message
 *   The message to be printed. If message is empty/null function
 *   will print new line.
 * @param bool $section
 *   Printout a section style.
 */
function uwprint(string $message = NULL, bool $section = FALSE) {
  if ($section && $message) {
    echo PHP_EOL;
    echo '*****************************************' . PHP_EOL;
    echo $message . PHP_EOL;
    echo '*****************************************' . PHP_EOL;
  }
  else {
    if ($message) {
      echo $message;
    }
  }

  echo PHP_EOL;
}
