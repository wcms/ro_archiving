<?php

// Step through all the years and fix the relative link.
for ($year = 2007; $year <= 2023; $year++) {

  // Reset the files array.
  $files = [];

  // Set the academic year based on the year.
  $academic_year = $year . '-' . ($year + 1);

  // Set the short academic year.
  $short_academic_year = $academic_year[2] . $academic_year[3] . $academic_year[7] . $academic_year[8];
  // Output current progress.
  uwprint('Starting ' . $academic_year . ' ...', TRUE);

  // Get the directories recursively.
  $rdi = new RecursiveDirectoryIterator(
    $academic_year,
    FilesystemIterator::KEY_AS_PATHNAME
  );

  // Step through each of the directories and get the files.
  foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {

    // If this is not a directory and not the . or .. file, then add
    // it to the files array.
    if (
      !is_dir($file) &&
      $info->getFileName() !== '.' &&
      $info->getFileName() !== '..'
    ) {

      $files[] = $file;
    }
  }

  // Step through each of the files and change the links.
  foreach ($files as $file) {

    // Output current progress.
    uwprint('Fixing ... ' . $file);

    // Get the actual page from the path.
    $page = file_get_contents($file);

    // Remove printable version links.
    $page = preg_replace(
      '/<a[^>]*?>Printable Version<\/a>\s*<a[^>]*?><img[^>]*?><\/a>/s',
      '',
      $page
    );

    // Remove broken image logo.
    $page = preg_replace(
      '/<img alt="The Undergraduate Calendar"[^>]*?>/s',
      '<p hspace="10" vspace="15" border="0" align="right">The Undergraduate Calendar </p>',
      $page
    );

    // Remove double .html.
    $page = str_replace(
      '.html.html',
      '.html',
      $page
    );

    // Remove space before .html.
    $page = str_replace(
      ' .html',
      '.html',
      $page
    );

    // Fix table links
    $page = str_replace(
      '#Table%20I.html',
      '.html#Table%20I',
      $page
    );
    $page = str_replace(
      '#Table%20I.htmlI',
      '.html#Table%20II',
      $page
    );

    // fix double academic year.
    $page = str_replace(
      '/' . $academic_year . '/' . $academic_year,
      '/' . $academic_year,
      $page
    );

    // fix software eng link (2021-2022 1)
    if ($file == '2021-2022/page/ENG-Deans-Honours-List.html') {
      $page = str_replace(
        '<a href="/2021-2022.html">Software Engineering</a>',
        '<a href="/undergraduate-studies/2021-2022/page/ENG-Software-Engineering.html">Software Engineering</a>',
        $page
      );
    }

    // fix intro link (2021-2022 2A)
    if ($file == '2021-2022/page/ENG-Examinations-and-Promotions-Rules.html') {
      $page = str_replace(
        '<a href="/2021-2022.html" shape="rect">Introduction</a>',
        '<a href="/undergraduate-studies/2021-2022/page/ENG-Examinations-and-Promotions-Introduction.html" shape="rect">Introduction</a>',
        $page
      );
    }

    // remove term deadline anchore. (2021-2022 2B)
    $page = str_replace(
      '#termdeadlines',
      '',
      $page
    );

    // Fix online link on chem eng. (2021-2022 3)
    $page = str_replace(
      '/?pageID=11090',
      '/page/uWaterloo-Centre-for-Extended-Learning',
      $page
    );

    // Fix anchor link. (2021-2022 4)
    $page = str_replace(
      '#communication.html',
      '.html#communication',
      $page
    );

    // Add anchors to anchor links. (2008-2009 2)
    if ($file == '2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering.html') {
      $page = str_replace(
        '.html">List A – Impact Courses</a>',
        '.html#ListA--ImpactCourses">List A – Impact Courses</a>',
        $page
      );
      $page = str_replace(
        '.html">List B – Engineering Economics Courses</a>',
        '.html#ListB--EngineeringEconomics">List B – Engineering Economics Courses</a>',
        $page
      );
      $page = str_replace(
        '.html">List C – Humanities and Social Sciences Courses.</a>',
        '.html#ListC--HumanitiesandSocialSciencesCourses">List C – Humanities and Social Sciences Courses.</a>',
        $page
      );
      $page = str_replace(
        '.html">A</a>',
        '.html#ListA--ImpactCourses">A</a>',
        $page
      );
      $page = str_replace(
        '.html">B</a>',
        '.html#ListB--EngineeringEconomics">B</a>',
        $page
      );
      $page = str_replace(
        '.html">C</a>',
        '.html#ListC--HumanitiesandSocialSciencesCourses">C</a>',
        $page
      );
      $page = str_replace(
        '.html">D</a>',
        '.html#ListD--OtherPermissibleComplementaryStudies">D</a>',
        $page
      );
    }

    preg_match_all(
      '/href="\/courses.aspx\?(.*?)"/s',
      $page,
      $hrefs
    );

    // Counter to be used for the search and replace patterns.
    $count = 0;
    $replacements = [];

    foreach ($hrefs[1] as $query) {
      $fixed_query = str_replace('amp;', '', $query);
      //uwprint($fixed_query, True);
      parse_str($fixed_query, $parameters);
      
      $keys = array_map('strtolower', array_keys($parameters));
      $parameters = array_combine($keys, array_values($parameters));
      //var_dump($parameters);

      // If this contains a level, code and number, we need
      // to get the correct redirect.
      // For example: /courses.aspx?Level=100&Code=ANTH&Number=201,
      // will go to: COURSE/course-ANTH.html#ANTH201
      if (
        in_array('level', $keys) &&
        in_array('code', $keys) &&
        in_array('number', $keys)
      ) {

        // Setup the correct redirect.
        $redirect = 'https://ucalendar.uwaterloo.ca/' . $short_academic_year;
        $redirect .= '/COURSE/course-' . $parameters['code'] . '.html';
        $redirect .= '#' . $parameters['code'] . $parameters['number'];

        // Set the search and replace.
        $replacements['href="/courses.aspx?' . $query . '"'] = 'href="' . $redirect . '"';

        // Increment the counter.
        $count++;
      }

      elseif (
        in_array('code', $keys) &&
        in_array('number', $keys)
      ) {

        // Setup the correct redirect.
        $redirect = 'https://ucalendar.uwaterloo.ca/' . $short_academic_year;
        $redirect .= '/COURSE/course-' . $parameters['code'] . '.html';
        $redirect .= '#' . $parameters['code'] . $parameters['number'];

        // Set the search and replace.
        $replacements['href="/courses.aspx?' . $query . '"'] = 'href="' . $redirect . '"';

        // Increment the counter.
        $count++;
      }

      // If this contains a level and code, we need
      // to get the correct redirect.
      // For example: /courses.aspx?Level=100&Code=ANTH,
      // will go to: COURSE/course-ANTH.html#ANTH100
      elseif (
        in_array('level', $keys) &&
        in_array('code', $keys)
      ) {

        // Setup the correct redirect.
        $redirect = 'https://ucalendar.uwaterloo.ca/' . $short_academic_year;
        $redirect .= '/COURSE/course-' . $parameters['code'] . '.html';
        $redirect .= '#' . $parameters['code'] . $parameters['level'] . 'S';

        // Set the search and replace.
        $replacements['href="/courses.aspx?' . $atag . '"'] = 'href="' . $redirect . '"';

        // Increment the counter.
        $count++;
      }
    }

    // Step through all the searches and replaces, and replace
    // the tag with the redirect.
    //var_dump($replacements);
    foreach ($replacements as $search => $replace) {
      $page = str_replace(
        $search,
        $replace,
        $page
      );
    }

    $page = str_replace(
      '<a href="https://ucalendar.uwaterloo.ca/0809/COURSE/course-ENVSNumber=178.html#ENVSNumber=178100S">ENVS 178</a>',
      '<a href="https://ucalendar.uwaterloo.ca/0809/COURSE/course-ENVS.html#ENVS178">ENVS 178</a>',
      $page
    );

    // Open and write the file with updated data.
    $file = fopen($file, "w") or die("Unable to open file!");
    fwrite($file, $page);
    fclose($file);
  }

  // Output current progress.
  uwprint('Completed ... ' . $academic_year);
}

/**
 * Function to print a message.
 *
 * @param string|null $message
 *   The message to be printed. If message is empty/null function
 *   will print new line.
 * @param bool $section
 *   Printout a section style.
 */
function uwprint(string $message = NULL, bool $section = FALSE) {
  if ($section && $message) {
    echo PHP_EOL;
    echo '*****************************************' . PHP_EOL;
    echo $message . PHP_EOL;
    echo '*****************************************' . PHP_EOL;
  }
  else {
    if ($message) {
      echo $message;
    }
  }

  echo PHP_EOL;
}
