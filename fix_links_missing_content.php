<?php

// Step through all the years and fix the links in the missing content.
for ($year = 2007; $year <= 2023; $year++) {

  // Reset the files array.
  $files = [];

  // Set the academic year based on the year.
  $academic_year = $year . '-' . ($year + 1);

  // Set the short academic year.
  $short_academic_year = $academic_year[2] . $academic_year[3] . $academic_year[7] . $academic_year[8];

  // Get the directories recursively.
  $rdi = new RecursiveDirectoryIterator(
    $academic_year,
    FilesystemIterator::KEY_AS_PATHNAME
  );

  // Step through each of the directories and get the files.
  foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {

    // If this is not a directory and not the . or .. file, then add
    // it to the files array.
    if (
      !is_dir($file) &&
      $info->getFileName() !== '.' &&
      $info->getFileName() !== '..'
    ) {

      $files[] = $file;
    }
  }

  // Step through each of the files and change the links.
  foreach ($files as $file) {
  
    $page = file_get_contents($file);

    // Get all the "a" tags on the page.
    preg_match_all(
        '/<\s*a[^>]*href="(.*?)"\s?(.*?)>/s',
        $page,
        $atags
    );

    // Counter to be used for the search and replace patterns.
    $count = 0;
    $replacements = [];

    // Step through all the "a" tags and correct them.
    foreach ($atags[1] as $atag) {

    // Reset the parameters array.
    $parameters = [];

    // If this is a tag with courses.aspx, we need to generate
    // the redirected url using the pattern that the acms
    // generates.
    if (str_contains($atag, 'courses.aspx')) {

      // For some reason some tags have amp; in it, so
      // just remove it.
      $fixed_atag = str_replace('amp;', '', $atag);
      $fixed_atag = str_replace(':///', '://', $fixed_atag);

      // Use parse_url() function to parse the URL
      // and return an associative array which
      // contains its various components.
      $url_components = parse_url($fixed_atag);

      // Use parse_str() function to parse the
      // string passed via URL.
      parse_str($url_components['query'], $parameters);

      // Get the array keys of the parameters.
      $keys = array_map('strtolower', array_keys($parameters));
      $parameters = array_combine($keys, array_values($parameters));

      // If this contains a level, code and number, we need
      // to get the correct redirect.
      // For example: /courses.aspx?Level=100&Code=ANTH&Number=201,
      // will go to: COURSE/course-ANTH.html#ANTH201
      if (
        in_array('level', $keys) &&
        in_array('code', $keys) &&
        in_array('number', $keys)
      ) {

        // Setup the correct redirect.
        $redirect = 'https://ucalendar.uwaterloo.ca/' . $short_academic_year;
        $redirect .= '/COURSE/course-' . $parameters['code'] . '.html';
        $redirect .= '#' . $parameters['code'] . $parameters['number'];

        // Set the search and replace.
        $replacements['href="' . $atag . '"'] = 'href="' . $redirect . '"';

        // Increment the counter.
        $count++;
      }

      // If this contains a level and code, we need
      // to get the correct redirect.
      // For example: /courses.aspx?Level=100&Code=ANTH,
      // will go to: COURSE/course-ANTH.html#ANTH100
      elseif (
        in_array('level', $keys) &&
        in_array('code', $keys)
      ) {

        // Setup the correct redirect.
        $redirect = 'https://ucalendar.uwaterloo.ca/' . $short_academic_year;
        $redirect .= '/COURSE/course-' . $parameters['code'] . '.html';
        $redirect .= '#' . $parameters['code'] . $parameters['level'] . 'S';

        // Set the search and replace.
        $replacements['href="' . $atag . '"'] = 'href="' . $redirect . '"';

        // Increment the counter.
        $count++;
      }
    }

    // If the tag contains a link directly to the courses, we need
    // to generate the correct url.
    // Example 1: /courses/CLAS/221
    // Goes to: COURSE/course-CLAS.html#CLAS221.
    // Example 2: /courses/ANTH/level/300
    // Goes to: COURSE/course-ANTH.html#ANTH300S.
    // Example 3: /courses/AFM.
    // Goes to: COURSE/course-AFM.html
    elseif (str_contains($atag, '/courses/')) {

      // Get the parts of the tag.
      $parts = explode('/', str_replace(
        ['https://ugradcalendar.uwaterloo.ca', '/' . $academic_year, '.html'],
        '',
        $atag
      ));

      // Setup the first part of the redirect, this will be the same
      // for examples 1/2 and is the final redirect for example 3.
      $redirect = 'https://ucalendar.uwaterloo.ca/' . $short_academic_year;
      $redirect .= '/COURSE/course-' . $parts[2] . '.html';

      // If there are 4 parts it is like example 1, so set the
      // correct redirect.
      if (count($parts) == 4) {
        $redirect .= '#' . $parts[2] . $parts[3];
      }

      // If there are 5 parts, it is like example 2, so set the
      // correct redirect.
      if (count($parts) == 5) {
        $redirect .= '#' . $parts[2] . $parts[4] . 'S';
      }

      // Set the search and replace.
      $replacements['href="' . $atag . '"'] = 'href="' . $redirect . '"';

      // Increment the counter.
      $count++;
    }

    // If tis is a tag contains https://ugradcalendar.uwaterloo.ca
    // replace it with /$academic_year.
    elseif (str_contains($atag, 'https://ugradcalendar.uwaterloo.ca')) {

      // Set the redirect.
      $redirect = str_replace(
        'https://ugradcalendar.uwaterloo.ca',
        '/undergraduate-studies/' . $academic_year,
        $atag
      );

      // Set the search and replace.
      $replacements['href="' . $atag . '"'] = 'href="' . $redirect . '.html"';

      // Increment the counter.
      $count++;
    }

    elseif (str_starts_with($atag, '/page/') || str_starts_with($atag, '/group/')) {
        $relacement['href="' . $atag . '"'] = 'href="/undergraduate-studies/' . $academic_year . $atag .'html"';
    }
  }

  // Step through all the searches and replaces, and replace
  // the tag with the redirect.
  foreach ($replacements as $search => $replace) {
    $page = str_replace(
      $search,
      $replace,
      $page
    );
  }

  // Open and write the file with updated data.
  if ($count > 0) {
    $file = fopen($file, "w") or die("Unable to open file!");
    fwrite($file, $page);
    fclose($file);
  }
  }
}