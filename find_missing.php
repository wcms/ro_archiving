<?php

// Step through all the years and fix the relative link.
for ($year = 2007; $year <= 2023; $year++) {

  // Reset the files array.
  $files = [];

  // Set the academic year based on the year.
  $academic_year = $year . '-' . ($year + 1);

  // Set the short academic year.
  $short_academic_year = $academic_year[2] . $academic_year[3] . $academic_year[7] . $academic_year[8];

  // Get the directories recursively.
  $rdi = new RecursiveDirectoryIterator(
    $academic_year,
    FilesystemIterator::KEY_AS_PATHNAME
  );

  // Step through each of the directories and get the files.
  foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {

    // If this is not a directory and not the . or .. file, then add
    // it to the files array.
    if (
      !is_dir($file) &&
      $info->getFileName() !== '.' &&
      $info->getFileName() !== '..'
    ) {

      $files[] = $file;
    }
  }

  // Step through each of the files and change the links.
  foreach ($files as $file) {

    if (str_contains($file, '/page/')) {
      // Get the actual page from the path.
      $page = file_get_contents($file);

      if (preg_match(
        '/Office of the Registrar.*?519-888-4567/s',
        $page)) {
      }if (str_contains($page, 'Office of the Registrar')){
      } else {
        $f = 'PAGES.txt';
        $current = file_get_contents($f);
        $current .= $file . "\n";
        file_put_contents($f, $current);
      }
    }
  }
}