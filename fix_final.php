<?php

// Get all the files.
$files = get_files();

//$files['2008-2009'][] = '2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering.html';
//$files['2017-2018'][] = '2017-2018/page/uWaterloo-Undergraduate-Calendar-Access.html';

// Step through each of the files and change the links.
foreach ($files as $academic_year => $a) {

  // Output current progress.
  uwprint('Starting ' . $academic_year . ' ...', TRUE);
  uwprint('');

  foreach ($a as $file) {

    // Output current progress.
    uwprint('Fixing ... ' . $file);

    // Get the actual page from the path.
    $page = file_get_contents($file);

    $page = fix_2018_2019_pages($page);

    // Write the file.
    $file = fopen($file, "w") or die("Unable to open file!");
    fwrite($file, $page);
    fclose($file);
  }
}

/**
 * Function to get the files.
 *
 * @return array
 *   The array of files.
 */
function get_files(): array {

  // Reset the files array.
  $files = [];

  // Step through all the years and fix the relative link.
  for ($year = 2007; $year <= 2023; $year++) {

    // Set the academic year based on the year.
    $academic_year = $year . '-' . ($year + 1);

    // Get the directories recursively.
    $rdi = new RecursiveDirectoryIterator(
      $academic_year,
      FilesystemIterator::KEY_AS_PATHNAME
    );

    // Step through each of the directories and get the files.
    foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {

      // If this is not a directory and not the . or .. file, then add
      // it to the files array.
      if (
        !is_dir($file) &&
        $info->getFileName() !== '.' &&
        $info->getFileName() !== '..'
      ) {

        $files[$academic_year][] = $file;
      }
    }
  }

  return $files;
}

/**
 * Function to make some fixes for 2008-2009.
 *
 * @param string $page
 *   The html to be fixed.
 *
 * @return string
 *   The fixed html.
 */
function fix_2018_2019_pages(
  string $page
): string {

//  // Fixes for links.
//  $page = str_replace(
//    '<a href="/2008-2009.html">Psychology Major</a>',
//    '<a href="/undergraduate-studies/2008-2009/page/441.html">Psychology Major</a>',
//    $page
//  );
//
//  // Fixes for links.
//  $page = str_replace(
//    '<a href="/2008-2009.html">Honours Psychology BA</a>',
//    '<a href="/undergraduate-studies/2008-2009/page/441.html">Honours Psychology BA</a>',
//    $page
//  );

//  $page = str_replace(
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html">List A – Impact Courses</a>',
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html#ListA--ImpactCourses">List A – Impact Courses</a>',
//    $page
//  );
//
//  $page = str_replace(
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html">List B – Engineering Economics Courses</a>',
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html#ListB--EngineeringEconomics">List B – Engineering Economics Courses</a>',
//    $page
//  );
//
//  $page = str_replace(
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html">List C – Humanities and Social Sciences Courses.</a>',
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html#ListC--HumanitiesandSocialSciencesCourses">List C – Humanities and Social Sciences Courses.</a>',
//    $page
//  );
//
//  $page = str_replace(
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html">A</a>',
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html#ListA--ImpactCourses">A</a>',
//    $page
//  );
//
//  $page = str_replace(
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html">B</a>',
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html#ListB--EngineeringEconomics">B</a>',
//    $page
//  );
//
//  $page = str_replace(
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html">C</a>',
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html#ListC--HumanitiesandSocialSciencesCourses">C</a>',
//    $page
//  );
//
//  $page = str_replace(
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html">D</a>',
//    '<a href="/undergraduate-studies/2008-2009/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering
//.html#ListD--OtherPermissibleComplementaryStudies">D</a>',
//    $page
//  );

  $page = str_replace(
    '<a href="www.findoutmore.uwaterloo.ca/financing/scholarships.php">',
    '<a href="https://uwaterloo.ca/find-out-more/financing/scholarships">',
    $page
  );

  return $page;
}
/**
 * Function to remove the windows newline.
 *
 * @param string $page
 *   The html to be fixed.
 * @param string $academic_year
 *   The academic year.
 *
 * @return string
 *   The fixed html.
 */
function remove_windows_newline(
  string $page,
  string $academic_year
): string {

  $page = str_ireplace('\x0D', '', $page);

  return $page;
}

/**
 * Function to fix hash tag links.
 *
 * @param string $page.
 *   The html to be fixed.
 * @param sring $academic_year
 *   The academic year
 *
 * @return string
 *   The fixed html.
 */
function fix_hash_tag_links(
  string $page,
  string $academic_year
): string {

  // Pattern for regular expression.
  $pattern = '/href="\/(.*?)"/s';

  // Find all links on the page.
  preg_match_all(
    $pattern,
    $page,
    $matches
  );

  // Step through the matches and check for any links that
  // have has tag in wrong place and fix.
  foreach ($matches[1] as $match) {

    // If the link has a has tag and its not at the end
    // of the link, then fix it.
    if (str_contains($match, '#') == TRUE) {
      if (str_ends_with($match, '.html')) {

        // Break apart the link on the hash tag, to grab
        // just the anchor link.
        $parts = explode('#', $match);

        // Remove the .html to get just the anchor.
        $anchor = str_replace('.html', '', $parts[1]);

        // Get the search and replace strings.
        $search = 'href="/' . $match . '"';
        $replace = str_replace('#' . $anchor, '', $match);
        $replace .= '#' . $anchor;
        $replace = 'href="/' . $replace . '"';

        // Perform the search and replace.
        $page = str_replace(
          $search,
          $replace,
          $page
        );
      }
    }
  }

  return $page;
}

/**
 * Function to fix group id links.
 *
 * @param string $page.
 *   The html to be fixed.
 * @param sring $academic_year
 *   The academic year
 *
 * @return string
 *   The fixed html.
 */
function fix_group_id_links(
  string $page,
  string $academic_year
): string {

  // Pattern for regular expression.
  $pattern = '/href="\/(.*?)"/s';

  // Find all links on the page.
  preg_match_all(
    $pattern,
    $page,
    $matches
  );

  // Counter to get right links.
  $count = 0;

  // Step through the matches and check for any links that
  // have spaces.
  foreach ($matches[1] as $match) {
    if (str_contains($match, 'Group ID') == TRUE) {
      var_dump($match);
    }
  }

  return $page;
}

/**
 * Function to fix spaces in links.
 */
function fix_space_in_links(
  string $page,
  string $academic_year
): string {

  // Pattern for regular expression.
  $pattern = '/href="\/(.*?)"/s';

  // Find all links on the page.
  preg_match_all(
    $pattern,
    $page,
    $matches
  );

  // Counter to get right links.
  $count = 0;

  // Step through the matches and check for any links that
  // have spaces.
  foreach ($matches[1] as $match) {

    // If there is a space in the link, remove it.
    if (
      !str_contains($page, 'Group ID') &&
      strpos($match, ' ') == true
    ) {
      $page = str_replace(
        $matches[0][$count],
        str_replace(' ', '', $matches[0][$count]),
        $page
      );
    }

    // Increment the count so we get correct links.
    $count++;
  }

  return $page;
}

/**
 * Function to remove the footer.
 *
 * @param string $page
 *   The html to be fixed.
 *
 * @return string
 *   The fixed html.
 */
function remove_footer(
  string $page,
  string $academic_year
): string {

  // Patterns to search for.
  $patterns = [
    '/(?<=)<p><a.*?Link to University of Waterloo home.*?<\/p>/s',
    '/(?<=)<td colspan="2">.*?Link to University of Waterloo home.*?<\/td>/s',
    '/(?<=)<td colspan="2">.*?href="http:\/\/www.uwaterloo.ca".*?<\/td>/s'
  ];

  // Step through each pattern and replace.
  foreach ($patterns as $pattern) {

    // Find all the occurrences of the pattern.
    preg_match_all(
      $pattern,
      $page,
      $matches
    );

    // If there are matches, remove them.
    if (count($matches[0])) {
      $page = str_replace(
        $matches[0][0],
        '',
        $page
      );
    }
  }

  $searches[] = '<a href="/' . $academic_year . '.html">contact us</a> |';
  $searches[] = '<a href="http://www.uwaterloo.ca">www.uwaterloo.ca</a> | ';
  $searches[] = 'powered by <a href="http://www.interglobal.ca/">InterGlobal Solutions</a> |';
  $searches[] = '&nbsp;<a href="http://www.uwaterloo.ca/privacy/index.html">privacy</a>';
  $searches[] = '<a href="http://www.uwaterloo.ca/privacy/index.html">privacy</a>';

  foreach ($searches as $search) {
    $page = str_replace(
      $search,
      '',
      $page
    );
  }

  return $page;
}

/**
 * Function to fix links that have no link to ugrad and academic year and no html.
 *
 * @param string $page
 *   The html to be fixed.
 * @param string $academic_year
 *   The academic year.
 *
 * @return string
 *   The fixed html.
 */
function fix_no_link_html_ext(
  string $page,
  string $academic_year
): string {

  // Pattern for regular expression.
  $pattern = '/href="\/.*?"/s';

  // Find all links on the page.
  preg_match_all(
    $pattern,
    $page,
    $matches
  );

  // Reset replacements array.
  $replacements = [];

  // Step through all the matches and correct.
  foreach ($matches[0] as $match) {

    // If the link does not have the undergraduate and academic year,
    // and is not App_Theme and is missing a .html and is not to the
    // root, then get the replacement, so it should only match something
    // /ARTS-Academic-Plans, and nothing else.
    if (
      !str_contains(
        $match,
        '/undergraduate-studies/' . $academic_year . '/',
      ) &&
      !str_contains(
        $match,
        '/App_Themes/',
      ) &&
      !str_contains(
        $match,
        '.html',
      ) &&
      $match !== 'href="/"'
    ) {

      // The start of the replacement string.
      $replace = str_replace('href="/', '', $match);
      $replace = str_replace('"', '', $replace);

      // Get the path to the file to try using the page directory.
      $path = $academic_year . '/page/' . $replace . '.html';

      // If the file does not exists, it should be in the group
      // directory but we are going to check again
      if (!file_exists($path)) {

        // Path to the file using the group directory.
        $path = $academic_year . '/group/' . $replace . '.html';

        // If there is no path, then set to NULL and we will
        // completely remove the link.
        if (!file_exists($path)) {
          $path = NULL;
        }
      }

      // If the path is null, then remove the link.
      // If not null then the replacement is with the file
      // name with undergraduate studies and academic year.
      if (!$path) {

        // The pattern for the null file.
        $null_pattern = '/<a href="\/' . $replace . '"[^>]*?>(.*?)<\/a>/s';

        // Find all links on the page with that link.
        preg_match_all(
          $null_pattern,
          $page,
          $null_matches
        );

        // Counter to get correct file name.
        $count = 0;

        // Step through the null matches and get replacement.
        foreach ($null_matches[0] as $null_match) {

          // The replacement.
          $replacements[$null_match] = $null_matches[1][$count];

          // Increment the counter so we get correct file.
          $count++;
        }
      }
      else {
        $replacements[$match] = 'href="/undergraduate-studies/' . $path . '"';
      }
    }
  }

  // Step through each of the replacements and perform the
  // search and replace.
  foreach ($replacements as $search => $replace) {
    $page = str_replace(
      $search,
      $replace,
      $page
    );
  }

  return $page;
}

/**
 * Function to correct blank links.
 *
 * @param string $page
 *   The html to be fixed.
 * @param string $academic_year
 *   The academic year.
 *
 * @return string
 *   The fixed html.
 */
function fix_blank_links(
  string $page,
  string $academic_year
): string {

  // Pattern for regular expression.
  $pattern = '/<a href=.*(?<!undergraduate-studies)\/' . $academic_year . '\/(.*?)\.html">/';

  // Find all the blank links.
  preg_match_all(
    $pattern,
    $page,
    $matches
  );

  // Counter to ensure correct replacement is used.
  $counter = 0;

  // Step through each of the matches and fix the link.
  foreach ($matches[0] as $match) {

    // The replacement string.
    $replace = '<a href="/undergraduate-studies/' . $academic_year . '/';
    $replace .= $matches[1][$counter] . '.html">';

    // Perform the search and replace.
    $page = str_replace(
      $match,
      $replace,
      $page
    );

    $counter++;
  }

  return $page;
}

/**
 * Function to get fix the cse links.
 *
 * @param string $page
 *   The html to be fixed.
 * @param string $academic_year
 *   The academic year.
 *
 * @return array|string|string[]
 *   The fixed html.
 */
function fix_cse_links(
  string $page,
  string $academic_year
) {

  // Reset matches array.
  $matches = [];

  // Get the matches to the CSE links.
  preg_match_all(
    '/<a href="\/(.*?).html".*>CSE<\/a>/',
    $page,
    $matches
  );

  // Counter to find all the matches.
  $counter = 0;

  // Step through each of the matches and fix the links.
  foreach ($matches[0] as $match) {

    // Replacement pattern.
    $replace = $match;
    $replace = str_replace(
      '/' . $matches[1][$counter] . '.html',
      '/undergraduate-studies/' . $academic_year . '/page/ENG-BASc-and-BSE-Complementary-Studies-Engineering.html',
      $replace
    );

    // Perform the search and replace.
    $page = str_replace(
      $match,
      $replace,
      $page
    );

    $counter++;
  }

  return $page;
}

/**
 * Function to fix hashtag rule links.
 *
 * @param string $page
 *   The html to be fixed.
 * @param string $academic_year
 *   The academic year.
 *
 * @return string
 *   The fixed html.
 */
function fix_hash_tag_rule_links(
  string $page,
  string $academic_year
): string {

  // Reset the matches array.
  $matches = [];

  // Regular expression pattern to look for.
  $pattern = '<a.*?href="\/undergraduate-studies\/';
  $pattern .= $academic_year;
  $pattern .= '\/page\/ENG-Examinations-and-Promotions-Rules\/##Rule.*?">(.*?)<\/a>';

  // Get all the matches based on the pattern.
  preg_match_all(
    '/' . $pattern . '/',
    $page,
    $matches
  );

  // Step through each of the matches and fix the hash tags.
  foreach ($matches[1] as $match) {

    // Break apart the match, so Rule 12 will be 0 => Rule and 1 => 12.
    $parts = explode(' ', $match);

    // If the second part is less than 10, then get the search
    // and replace based on that, if not less than 10, there is
    // a weird search, meaning Rule_1.html2, so have to get
    // the search pattern based on that.
    if ($parts[1] < 10) {
      $search = '/undergraduate-studies/' . $academic_year;
      $search .= '/page/ENG-Examinations-and-Promotions-Rules/##Rule_';
      $search .= $parts[1] . '.html';

      $replace = '/undergraduate-studies/' . $academic_year;
      $replace .= '/page/ENG-Examinations-and-Promotions-Rules.html##';
      $replace .= 'Rule_' . $parts[1];
    }

    else {

      $search = '/undergraduate-studies/' . $academic_year;
      $search .= '/page/ENG-Examinations-and-Promotions-Rules/##Rule_';
      $search .= $parts[1][0] . '.html' . $parts[1][1];

      $replace = '/undergraduate-studies/' . $academic_year;
      $replace .= '/page/ENG-Examinations-and-Promotions-Rules.html##';
      $replace .= 'Rule_' . $parts[1];
    }

    // Perform the search and replace.
    $page = str_replace(
      $search,
      $replace,
      $page
    );
  }

  return $page;
}

/**
 * Function to remove the extra lis.
 *
 * @param string $page
 *   The html to be fixed.
 *
 * @return string
 *   The fixed html.
 */
function remove_extra_lis(string $page): string {

  // Remove extra a li li li.
  $page = str_replace(
    '</a></li></li></li>',
    '</a></li></ul>',
    $page
  );

  // Remove extra a li li.
  $page = str_replace(
    '</a></li></li>',
    '</a></li>',
    $page
  );

  // Remove extra ul li.
  $page = str_replace(
    "<ul style='margin: 0 0 0 10px;padding: 0; display: block;list-style-type:none;display: block;'></li>",
    "<ul style='margin: 0 0 0 10px;padding: 0; display: block;list-style-type:none;display: block;'>",
    $page
  );

  return $page;
}

/**
 * Function to print a message.
 *
 * @param string|null $message
 *   The message to be printed. If message is empty/null function
 *   will print new line.
 * @param bool $section
 *   Printout a section style.
 */
function uwprint(string $message = NULL, bool $section = FALSE) {
  if ($section && $message) {
    echo PHP_EOL;
    echo '*****************************************' . PHP_EOL;
    echo $message . PHP_EOL;
    echo '*****************************************' . PHP_EOL;
  }
  else {
    if ($message) {
      echo $message;
    }
  }

  echo PHP_EOL;
}
