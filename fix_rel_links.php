<?php

// Step through all the years and fix the relative link.
for ($year = 2007; $year <= 2023; $year++) {

  // Reset the files array.
  $files = [];

  // Set the academic year based on the year.
  $academic_year = $year . '-' . ($year + 1);

  // Output current progress.
  uwprint('Starting ' . $academic_year . ' ...', TRUE);

  // Get the directories recursively.
  $rdi = new RecursiveDirectoryIterator(
    $academic_year,
    FilesystemIterator::KEY_AS_PATHNAME
  );

  // Step through each of the directories and get the files.
  foreach (new RecursiveIteratorIterator($rdi, RecursiveIteratorIterator::SELF_FIRST) as $file => $info) {

    // If this is not a directory and not the . or .. file, then add
    // it to the files array.
    if (
      !is_dir($file) &&
      $info->getFileName() !== '.' &&
      $info->getFileName() !== '..'
    ) {

      $files[] = $file;
    }
  }

  // Step through each of the files and change the links.
  foreach ($files as $file) {

    // Output current progress.
    uwprint('Fixing ... ' . $file);

    // Get the actual page from the path.
    $page = file_get_contents($file);

    // Search and replace patterns.
    $search = 'href="/' . $academic_year . '/';
    $replace = 'href="/undergraduate-studies/' . $academic_year . '/';

    // Perform the search and replace.
    $page = str_replace(
      $search,
      $replace,
      $page
    );

    // Open and write the file with updated data.
    $file = fopen($file, "w") or die("Unable to open file!");
    fwrite($file, $page);
    fclose($file);
  }

  // Output current progress.
  uwprint('Completed ... ' . $academic_year);
}

/**
 * Function to print a message.
 *
 * @param string|null $message
 *   The message to be printed. If message is empty/null function
 *   will print new line.
 * @param bool $section
 *   Printout a section style.
 */
function uwprint(string $message = NULL, bool $section = FALSE) {
  if ($section && $message) {
    echo PHP_EOL;
    echo '*****************************************' . PHP_EOL;
    echo $message . PHP_EOL;
    echo '*****************************************' . PHP_EOL;
  }
  else {
    if ($message) {
      echo $message;
    }
  }

  echo PHP_EOL;
}
